# docker-razorfy
Alpine based Dockerfile to run razor-check [razor](https://razor.sourceforge.net/) via inetd as it is recommended to call it directly now instead of using razorfy.

[![Docker Pulls](https://img.shields.io/docker/pulls/a16bitsysop/razorfy.svg?style=plastic)](https://hub.docker.com/r/a16bitsysop/razorfy/)
[![Docker Stars](https://img.shields.io/docker/stars/a16bitsysop/razorfy.svg?style=plastic)](https://hub.docker.com/r/a16bitsysop/razorfy/)
[![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/a16bitsysop/razorfy/latest?style=plastic)](https://hub.docker.com/r/a16bitsysop/razorfy/)
[![Release Commit SHA](https://img.shields.io/badge/dynamic/json.svg?label=release%20commit%20SHA&style=plastic&color=orange&query=sha&url=https://gitlab.com/container-email/razorfy/-/raw/main/badges.json)](https://gitlab.com/container-email/razorfy/)

Edit /home/razor/.razor/razor-agent.conf to enable logging to a file if needed.

## GitLab
GitLab Repository: [https://gitlab.com/container-email/razorfy](https://gitlab.com/container-email/razorfy)


## Environment Variables
| Name                | Desription                                              | Default   |
| ------------------- | ------------------------------------------------------- | --------- |
| TIMEZONE            | Timezone to use inside the container, eg: Europe/London | unset     |

## Examples
**To run a container exposing razorfy port with verbose logging**
```bash
#docker container run -p 11342:11342 -d --name razorfy a16bitsysop/razorfy
```
