ARG DVER=latest
FROM docker.io/alpine:$DVER
LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"
ARG APKVER
ENV url=HeinleinSupport/razorfy/master/razorfy.pl

# hadolint ignore=DL3018
RUN adduser -h /home/razor -D razor --gecos "razor antispam" 2>/dev/null \
&& install -d --owner=razor --group=razor /home/razor/.razor \
&& apk update \
&& apk upgrade --available --no-cache \
&& apk add -u --no-cache busybox-extras razor \
&& echo "razor-check 11342/tcp" >>/etc/services

WORKDIR /home/razor/.razor
COPY --chmod=644 --chown=razor:razor razor-agent.conf .

WORKDIR /etc
COPY --chmod=644 inetd.conf .

WORKDIR /usr/local/bin
COPY --chmod=755 container-scripts/set-timezone.sh entrypoint.sh razorfy.sh ./

CMD [ "entrypoint.sh" ]
EXPOSE 11342
