#!/bin/sh
# display environment variables passed with --env
echo "\$TIMEZONE= $TIMEZONE"
echo

NME=razor
set-timezone.sh "$NME"

echo "Starting inetd as user $NME"
su -c '/usr/sbin/inetd -f /etc/inetd.conf' - "$NME"
